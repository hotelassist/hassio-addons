CONFIG_PATH=/data/options.json

NGROK_AUTH="$(jq --raw-output '.ngrok.auth' $CONFIG_PATH)"
NGROK_TOKEN="$(jq --raw-output '.ngrok.token' $CONFIG_PATH)"
NGROK_PROTO="$(jq --raw-output '.ngrok.proto' $CONFIG_PATH)"
TARGET_HOST="$(jq --raw-output '.hook.host' $CONFIG_PATH)"
TARGET_PORT="$(jq --raw-output '.hook.port' $CONFIG_PATH)"
SLACK_WEBHOOK_URL="$(jq --raw-output '.slack.webhookURL' $CONFIG_PATH)"
SLACK_CHANNEL="$(jq --raw-output '.slack.channel' $CONFIG_PATH)"

PARAMS=""

if [ ! -z "$NGROK_AUTH" ]; then
    PARAMS="$PARAMS NGROK_AUTH=$NGROK_AUTH"
fi

if [ ! -z "$NGROK_TOKEN" ]; then
    echo "authtoken: $NGROK_TOKEN" >> /ngrok.yml
    cat /ngrok.yml
    # PARAMS="$PARAMS NGROK_TOKEN=$NGROK_TOKEN"
fi

if [ ! -z "$NGROK_PROTO" ]; then
    PARAMS="$PARAMS NGROK_PROTO=$NGROK_PROTO"
fi

if [ ! -z "$TARGET_HOST" ]; then
    PARAMS="$PARAMS TARGET_HOST=$TARGET_HOST"
fi

if [ ! -z "$TARGET_PORT" ]; then
    PARAMS="$PARAMS TARGET_PORT=$TARGET_PORT"
fi

if [ ! -z "$SLACK_WEBHOOK_URL" ]; then
    PARAMS="$PARAMS SLACK_WEBHOOK_URL=$SLACK_WEBHOOK_URL"
fi

if [ ! -z "$SLACK_CHANNEL" ]; then
    PARAMS="$PARAMS SLACK_CHANNEL=$SLACK_CHANNEL"
fi

echo "$PARAMS npm start"
sh -c "$PARAMS npm start"
