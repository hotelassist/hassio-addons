const ngrok = require("ngrok");
const slack = require('./slack');

if (!process.env.TARGET_HOST || !process.env.TARGET_PORT) {
  if(process.env.SLACK_WEBHOOK_URL) {
    slack.notify(
      process.env.SLACK_WEBHOOK_URL,
      {
        text: "The following env variables are required: TARGET_HOST, TARGET_PORT",
        channel: '#ngrok'
      }
    )
    .catch(err => console.error(err))
    .then(() => {
        throw new Error(
          "The following env variables are required: TARGET_HOST, TARGET_PORT"
        );
    })
    .catch(e => {
        console.error(e);
        process.exit(1);
    })
  }
}

let slackChannelEnv;
if (process.env.SLACK_CHANNEL && process.env.SLACK_CHANNEL.length) {
  slackChannelEnv = process.env.SLACK_CHANNEL.replace('#','')
}

const slackChannel = `#${slackChannelEnv || 'ngrok'}`;

const notify = (text, channel) => {
  if(process.env.SLACK_WEBHOOK_URL) {
    slack.notify(process.env.SLACK_WEBHOOK_URL, { text, channel });
  }
};

const targetAddress = `${process.env.TARGET_HOST}:${process.env.TARGET_PORT}`;

const validRegions = ["us", "eu", "au", "ap"];
const region = process.env.NGROK_REGION
  ? validRegions.includes(process.env.NGROK_REGION.toLowerCase())
    ? process.env.NGROK_REGION.toLowerCase()
    : "us"
  : "us";

const validProtos = ["http", "tcp"];
const proto = process.env.NGROK_PROTO
  ? validProtos.includes(process.env.NGROK_PROTO.toLowerCase())
    ? process.env.NGROK_PROTO.toLowerCase()
    : "http"
  : "http";

const options = {
  proto: proto,
  addr: targetAddress,
  region: region,
  // authtoken: process.env.NGROK_TOKEN,
  configPath: "/ngrok.yml"
};

if(process.env.NGROK_AUTH) {
  options.auth = process.env.NGROK_AUTH;
}

console.log({ options });

ngrok
  .connect(options)
  .then(url => {
    console.log(`The ngrok tunnel is active`);
    console.log(`${url} ---> ${targetAddress}`);
    notify(`The ngrok tunnel is active, ${url.replace(/tcp:\/\//i, 'http://')} ---> ${targetAddress}`, slackChannel);
  })
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
