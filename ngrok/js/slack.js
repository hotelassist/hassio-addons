function notify(hookUrl, message) {
  const SlackWebhook = require('slack-webhook');
  const slack = new SlackWebhook(hookUrl);
  return slack.send(message)
}

module.exports = {
  notify
}
