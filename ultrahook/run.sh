CONFIG_PATH=/data/options.json

API_KEY="$(jq --raw-output '.remote.apiKey' $CONFIG_PATH)"
SUBDOMAIN="$(jq --raw-output '.remote.subdomain' $CONFIG_PATH)"
WEBHOOK_HOST="$(jq --raw-output '.hook.host' $CONFIG_PATH)"
WEBHOOK_PATH="$(jq --raw-output '.hook.path' $CONFIG_PATH)"
WEBHOOK_ID="$(jq --raw-output '.hook.id' $CONFIG_PATH)"

/usr/local/bundle/bin/ultrahook -k $API_KEY $SUBDOMAIN $WEBHOOK_HOST/$WEBHOOK_PATH/$WEBHOOK_ID